import { Component, OnInit } from '@angular/core';
import {Pokemon} from '../../pokemon';
import {Router} from '@angular/router';
import {PokemonsService} from '../pokemons.service';

@Component({
  selector: 'app-list-pokemon',
  templateUrl: './list-pokemon.component.html',
  styleUrls: ['./list-pokemon.component.scss']
})
export class ListPokemonComponent implements OnInit {
  pokemons: Pokemon[] = [];

  constructor(private router: Router, private pokemonService: PokemonsService) { }

  ngOnInit(): void {
    this.pokemons = this.pokemonService.getListPokemons();
  }

  // tslint:disable-next-line:typedef
  selectPokemon(pokmn: Pokemon): void {
    // alert('Vous avez sélectionné: ' + pokmn.name);
    // Passer en paramètre l'url de redirection + les paramètres eventuels de la route.
    const link = ['/pokemon', pokmn.id];
    this.router.navigate(link);
  }

}
