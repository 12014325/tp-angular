import {Component, Input, OnInit} from '@angular/core';
import {Pokemon} from '../../pokemon';
import {Router} from '@angular/router';
import {PokemonsService} from '../pokemons.service';

@Component({
  selector: 'app-pokemon-form',
  templateUrl: './pokemon-form.component.html',
  styleUrls: ['./pokemon-form.component.scss']
})
export class PokemonFormComponent implements OnInit {

  @Input() pokemon: Pokemon | undefined;
  types: Array<string> | undefined;

  constructor(private  router: Router, private pokemonsService: PokemonsService) { }

  ngOnInit(): void {

    this.types = this.pokemonsService.getPokemonType();
  }

  /**
   * Détermine si le type passé en paramètre appartient ou non au pokémon
   * en cours d'édition
   * @param type
   */

  hasType(type: string): boolean {
    const index = this.pokemon.types.indexOf(type);
    return (index > -1) ? true : false;
  }

  /**
   * Méthode appelée lorsqu'un utilisateur ajoute ou retire un type au Pokémon
   * en cours d'édition
   * @param $event
   * @param type
   * @constructor
   */

  selectType($event: any, type: string): void {
    const checked = $event.target.checked;
    if (checked) {
      this.pokemon.types.push(type);
    } else {
      const index = this.pokemon.types.indexOf(type);
      if (index > -1) {
        this.pokemon.types.splice(index, 1);
      }
    }
  }

  /**
   * Méthode appelée lorsque le formulaire est soumis
   */

  onSubmit(): void {
    console.log('Submit form !');
    const link = ['/pokemon', this.pokemon.id];
    this.router.navigate(link);
  }

  /**
   * Valide le nombre de types pour chaque pokémon
   */

  isTypesValid(type: string): boolean {
    if (this.pokemon.types.length === 1 && this.hasType(type)) {
      return false;
    }
    if (this.pokemon.types.length >= 3 && this.hasType(type)) {
      return false;
    }
    return  true;
  }

}
